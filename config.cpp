////////////////////////////////////////////////////////////////////
//DeRap: config.bin
//Produced from mikero's Dos Tools Dll version 9.10
//https://mikero.bytex.digital/Downloads
//'now' is Tue Dec 03 15:09:00 2024 : 'file' last modified on Tue May 02 21:33:20 2023
////////////////////////////////////////////////////////////////////

#define _ARMA_

class CfgPatches
{
	class czzzz_DzrNotes
	{
		requiredAddons[] = {"DZ_Data","DZ_Gear_Consumables"};
		units[] = {"DZR_PaperWritten"};
		weapons[] = {};
	};
};
class CfgMods
{
	class czzzz_DzrNotes
	{
		type = "mod";
		author = "DayZRussia";
		description = "dzr_notes";
		dir = "dzr_notes";
		name = "dzr_notes";
		dependencies[] = {};
		class defs{};
	};
};
class CfgVehicles
{
	class Paper;
	class DZR_PaperWritten: Paper
	{
		canBeSplit = 0;
		varStackMax = 1;
		scope = 2;
		displayName = "$STR_DZR_Paper_Written";
		descriptionShort = "$STR_DZR_Paper_Written_DESC";
		model = "\dzr_notes\dzr_paper_written.p3d";
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\dzr_paper_co.paa"};
		inventorySlot[] = {"Paper","PaperWritten","DzrPaper1","DzrPaper2","DzrPaper3","DzrPaper4","DzrPaper5","DzrPaper6","DzrPaper7","DzrPaper8","DzrPaper9","DzrPaper11","DzrPaper12","DzrPaper13","DzrPaper14","DzrPaper15","DzrPaper16","DzrPaper17","DzrPaper18","DzrPaper19","DzrPaper20"};
	};
};
